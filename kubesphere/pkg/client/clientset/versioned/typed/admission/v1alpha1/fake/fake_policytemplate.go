/*
Copyright 2020 The KubeSphere Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	"context"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
	v1alpha1 "kubesphere.io/api/admission/v1alpha1"
)

// FakePolicyTemplates implements PolicyTemplateInterface
type FakePolicyTemplates struct {
	Fake *FakeAdmissionV1alpha1
}

var policytemplatesResource = schema.GroupVersionResource{Group: "admission.kubesphere.io", Version: "v1alpha1", Resource: "policytemplates"}

var policytemplatesKind = schema.GroupVersionKind{Group: "admission.kubesphere.io", Version: "v1alpha1", Kind: "PolicyTemplate"}

// Get takes name of the policyTemplate, and returns the corresponding policyTemplate object, and an error if there is any.
func (c *FakePolicyTemplates) Get(ctx context.Context, name string, options v1.GetOptions) (result *v1alpha1.PolicyTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(policytemplatesResource, name), &v1alpha1.PolicyTemplate{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PolicyTemplate), err
}

// List takes label and field selectors, and returns the list of PolicyTemplates that match those selectors.
func (c *FakePolicyTemplates) List(ctx context.Context, opts v1.ListOptions) (result *v1alpha1.PolicyTemplateList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(policytemplatesResource, policytemplatesKind, opts), &v1alpha1.PolicyTemplateList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.PolicyTemplateList{ListMeta: obj.(*v1alpha1.PolicyTemplateList).ListMeta}
	for _, item := range obj.(*v1alpha1.PolicyTemplateList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested policyTemplates.
func (c *FakePolicyTemplates) Watch(ctx context.Context, opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(policytemplatesResource, opts))
}

// Create takes the representation of a policyTemplate and creates it.  Returns the server's representation of the policyTemplate, and an error, if there is any.
func (c *FakePolicyTemplates) Create(ctx context.Context, policyTemplate *v1alpha1.PolicyTemplate, opts v1.CreateOptions) (result *v1alpha1.PolicyTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(policytemplatesResource, policyTemplate), &v1alpha1.PolicyTemplate{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PolicyTemplate), err
}

// Update takes the representation of a policyTemplate and updates it. Returns the server's representation of the policyTemplate, and an error, if there is any.
func (c *FakePolicyTemplates) Update(ctx context.Context, policyTemplate *v1alpha1.PolicyTemplate, opts v1.UpdateOptions) (result *v1alpha1.PolicyTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(policytemplatesResource, policyTemplate), &v1alpha1.PolicyTemplate{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PolicyTemplate), err
}

// Delete takes name of the policyTemplate and deletes it. Returns an error if one occurs.
func (c *FakePolicyTemplates) Delete(ctx context.Context, name string, opts v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteAction(policytemplatesResource, name), &v1alpha1.PolicyTemplate{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakePolicyTemplates) DeleteCollection(ctx context.Context, opts v1.DeleteOptions, listOpts v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(policytemplatesResource, listOpts)

	_, err := c.Fake.Invokes(action, &v1alpha1.PolicyTemplateList{})
	return err
}

// Patch applies the patch and returns the patched policyTemplate.
func (c *FakePolicyTemplates) Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts v1.PatchOptions, subresources ...string) (result *v1alpha1.PolicyTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(policytemplatesResource, name, pt, data, subresources...), &v1alpha1.PolicyTemplate{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PolicyTemplate), err
}
