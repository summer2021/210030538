## 集成Gatekeeper代码说明

本项目包含两个子项目的更改：

#### 1. kubesphere

kubesphere后端代码，实现新的admission相关资源和API，代码位于master的kubesphere文件夹内，

带有提交历史的分支为：[Files · kubesphere · Summer2021 / 210030538 · GitLab (summer-ospp.ac.cn)](https://gitlab.summer-ospp.ac.cn/summer2021/210030538/-/tree/kubesphere)



#### 2. ks-installer

kubesphere安装管理代码，实现gatekeeper的安装集成，代码位于master的ks-installer文件夹内，

带有提交历史的分支为：[Files · ks-installer · Summer2021 / 210030538 · GitLab (summer-ospp.ac.cn)](https://gitlab.summer-ospp.ac.cn/summer2021/210030538/-/tree/ks-installer)

